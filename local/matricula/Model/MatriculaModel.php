<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Model;

/**
 * Class HomeModel.
 * Convalidaciones
 * =======
 * Los convalidaciones se encarga de añadir una fecha de inicio y fin a cada grupo dentro de un curso.
 *
 * @copyright  2018 UPC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class MatriculaModel {

    /**
     * Método para cargar el objeto de solicitud por su ID
     * @global object $DB
     * @param int $id
     * @return object
     */
    public static function getMatriculasAll() {
        global $DB;
        $sql = "select m.*, "
                . " concat(a.chr_first_name,' ',a.chr_last_name) as alumno, "
                . " s.chr_name as semestre,"
                . " a.int_ciclo as ciclo, "
                . " c.chr_name as carrera, "
                . " from_unixtime(m.date_timecreated) as fecha,"
                . " (select count(t.id) from mdl_mtc_matricula_curso t  inner join mdl_mtc_matricula tt on tt.id=t.int_matriculaid where tt.int_alumnoid=a.id and tt.is_active=1 and tt.is_deleted=0 and t.is_active=1 and t.is_deleted=0) as num "
                . " from mdl_mtc_matricula m "
                . " inner join mdl_mtc_alumno a on a.id= m.int_alumnoid "
                . " inner join mdl_mtc_carrera c on c.id=a.int_carreraid  "
                . " inner join mdl_mtc_mst_semestre s on s.id= m.int_semestreid "
                . " left join mdl_mtc_matricula_curso mc on mc.int_matriculaid= m.id and mc.is_active=1 and mc.is_deleted=0"
                . " where m.is_active=1 and m.is_deleted=0";
        return $DB->get_records_sql($sql);
    }
    
    
    public static function getMatriculasAllSemestre($id) {
        global $DB;
        $sql = "select m.*, "
                . " concat(a.chr_first_name,' ',a.chr_last_name) as alumno, "
                . " s.chr_name as semestre,"
                . " a.int_ciclo as ciclo, "
                . " c.chr_name as carrera,"
                . " from_unixtime(m.date_timecreated) as fecha,"
                . " (select count(t.id) from mdl_mtc_matricula_curso t  inner join mdl_mtc_matricula tt on tt.id=t.int_matriculaid where tt.int_alumnoid=a.id and tt.is_active=1 and tt.is_deleted=0 and t.is_active=1 and t.is_deleted=0) as num "
                . " from mdl_mtc_matricula m "
                . " inner join mdl_mtc_alumno a on a.id= m.int_alumnoid "
                . " inner join mdl_mtc_mst_semestre s on s.id= m.int_semestreid "
                . " inner join mdl_mtc_carrera c on c.id=a.int_carreraid "
                . " left join mdl_mtc_matricula_curso mc on mc.int_matriculaid= m.id and mc.is_active=1 and mc.is_deleted=0  "
                . " where m.is_active=1 and m.is_deleted=0 and m.int_semestreid=$id " ;
        return $DB->get_records_sql($sql);
    }
    

    public static function getSemestresAll() {
        global $DB;
        $sql = "select m.* "
                . " from mdl_mtc_mst_semestre m "
                . " where m.is_active=1 and m.is_deleted=0 order by m.chr_code desc";
        return $DB->get_records_sql($sql);
    }

    public static function saveMatricula($obj) {
        global $DB;
        $returnValue = $DB->insert_record('mtc_matricula', $obj);
        return $returnValue;
    }

    public static function saveMatriculaCurso($obj) {
        global $DB;
        $returnValue = $DB->insert_record('mtc_matricula_curso', $obj);
        return $returnValue;
    }

    public static function getMatriculaById($id) {
        global $DB;
        $sql = "select * from mdl_mtc_matricula where is_active=1 and is_deleted=0 and id=$id";
        return $DB->get_record_sql($sql);
    }

    public static function updateMatricula($obj) {
        global $DB;
        $returnValue = $DB->update_record('mtc_matricula', $obj);
        return $returnValue;
    }

    public static function getCursosAllDisponibles($objMatricula) {
        global $DB;
        $alumno = AlumnoModel::getAlumnoById($objMatricula->int_alumnoid);

        if ($alumno) {
            $sql = "select c.* from mdl_mtc_curso c where c.is_active=1 and c.is_deleted=0 and c.int_carreraid=$alumno->int_carreraid and c.int_ciclo= $alumno->int_ciclo "
                    . "and c.id not in(select mc.int_cursoid from mdl_mtc_matricula_curso mc where mc.is_active=1 and mc.is_deleted=0 and mc.int_matriculaid=$objMatricula->id)"
                    . " and c.int_cantidad_alumnos > (select t.int_cantidad_ocupada from mdl_mtc_curso t where t.id=c.id) ";
            return $DB->get_records_sql($sql);
        } else {
            return null;
        }
    }

    public static function getCursosAllSeleccionados($objMatricula) {
        global $DB;
        $sql = "select * from mdl_mtc_curso c where c.is_active=1 and c.is_deleted=0 "
                . "and c.id in(select mc.int_cursoid from mdl_mtc_matricula_curso mc where mc.is_active=1 and mc.is_deleted=0 and mc.int_matriculaid=$objMatricula->id)";
        return $DB->get_records_sql($sql);
    }
    
     public static function UpdateAlumnoOcupado($idmatricula) {
        global $DB;
        $sql = "UPDATE mdl_mtc_alumno SET is_ocupado=0 where id in (select concat(int_alumnoid) from mdl_mtc_matricula where is_active=1 and is_deleted=0 and id= $idmatricula)";
        $DB->execute($sql);
        return $id;
    }
    
    
    public static function UpdateMatriculaCursoOcupado($idmatricula) {
        global $DB;
        $sql = "UPDATE mdl_mtc_matricula_curso SET is_active=0 , is_deleted= 1 where id in (select concat(id) from mdl_mtc_matricula_curso where is_active=1 and is_deleted=0 and int_matriculaid= $idmatricula)";
        $DB->execute($sql);
        return $id;
    }
    
    
     public static function UpdateCursoOcupado($idmatricula) {
        global $DB;
        $sql = "UPDATE mdl_mtc_curso SET is_ocupado=0 , int_cantidad_ocupada= (int_cantidad_ocupada-1) where id in (select concat(int_cursoid) from mdl_mtc_matricula_curso where is_active=1 and is_deleted=0 and int_matriculaid= $idmatricula)";
        $DB->execute($sql);
        return $id;
    }

    public static function updateMatriculaCurso($idmatricula,$idcurso) {
        global $DB;
        $sql = "UPDATE mdl_mtc_matricula_curso SET is_active=0 , is_deleted= 1 where id in (select concat(id) from mdl_mtc_matricula_curso where is_active=1 and is_deleted=0 and int_matriculaid= $idmatricula and int_cursoid=$idcurso)";
        $DB->execute($sql);
        return $id;
    }
    
    

}
