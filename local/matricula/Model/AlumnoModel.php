<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Model;

/**
 * Class HomeModel.
 * Convalidaciones
 * =======
 * Los convalidaciones se encarga de añadir una fecha de inicio y fin a cada grupo dentro de un curso.
 *
 * @copyright  2018 UPC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class AlumnoModel {

 
    /**
     * Método para cargar el objeto de solicitud por su ID
     * @global object $DB
     * @param int $id
     * @return object
     */
    public static function getAlumnosAll() {
        global $DB;
        $sql = "select a.*,"
                . " c.chr_name as carrera "
                . " from mdl_mtc_alumno a "
                . " inner join mdl_mtc_carrera c on c.id = a.int_carreraid "
                . " where a.is_active=1 and a.is_deleted=0";
        return $DB->get_records_sql($sql);
    }
    
    public static function getAlumnosAllCursoid($id) {
        global $DB;
        $sql = "select a.*,"
                . " c.chr_name as carrera "
                . " from mdl_mtc_alumno a "
                . " inner join mdl_mtc_carrera c on c.id = a.int_carreraid "
                . " inner join mdl_mtc_matricula m on m.int_alumnoid=a.id and m.is_active =1 and m.is_deleted=0 "
                . " inner join mdl_mtc_matricula_curso mc on mc.int_matriculaid=m.id and mc.int_cursoid= $id and mc.is_active =1 and mc.is_deleted=0 "
                . " where a.is_active=1 and a.is_deleted=0  group by a.id ";
        return $DB->get_records_sql($sql);
    }
    
    public static function getAlumnosAllNoOcupados() {
        global $DB;
        $sql = "select a.*,"
                . " c.chr_name as carrera "
                . " from mdl_mtc_alumno a "
                . " inner join mdl_mtc_carrera c on c.id = a.int_carreraid "
                . " where a.is_active=1 and a.is_deleted=0 and a.is_ocupado=0";
        return $DB->get_records_sql($sql);
    }
    
    
    public static function getCarrerasAll() {
        global $DB;
        $sql = "select *from mdl_mtc_carrera where is_active=1 and is_deleted=0";
        return $DB->get_records_sql($sql);
    }
    
    
    public static function getCiclosAll() {
        global $DB;
        $sql = "select *from mdl_mtc_mst_ciclo where is_active=1 and is_deleted=0";
        return $DB->get_records_sql($sql);
    }
    
    
    public static function getAlumnoById($id) {
        global $DB;
        $sql = "select * from mdl_mtc_alumno where is_active=1 and is_deleted=0 and id=$id";
        return $DB->get_record_sql($sql);
    }
    
    public static function getMatriculaidByAlumnoid($dni) {
        global $DB;
        $sql = "select id from mdl_mtc_matricula where is_active=1 and is_deleted=0 and int_alumnoid='".$dni."'";
        return $DB->get_field_sql($sql);
    }
    
    public static function getAlumnoBycode($dni) {
        global $DB;
        $sql = "select * from mdl_mtc_alumno where is_active=1 and is_deleted=0 and chr_cod_alumno='".$dni."'";
        return $DB->get_record_sql($sql);
    }
    
    
    public static function getAlumnoByCorreo($dni) {
        global $DB;
        $sql = "select * from mdl_mtc_alumno where is_active=1 and is_deleted=0 and chr_correo='".$dni."'";
        return $DB->get_record_sql($sql);
    }
    
    public static function getAlumnoByCelular($dni) {
        global $DB;
        $sql = "select * from mdl_mtc_alumno where is_active=1 and is_deleted=0 and int_celular='".$dni."'";
        return $DB->get_record_sql($sql);
    }
    
    public static function getAlumnoByDNI($dni) {
        global $DB;
        $sql = "select * from mdl_mtc_alumno where is_active=1 and is_deleted=0 and int_dni=$dni";
        return $DB->get_record_sql($sql);
    }
    
    public static function saveAlumno($obj) {
        global $DB;
        $returnValue = $DB->insert_record('mtc_alumno', $obj);
        return $returnValue;
    }
    
    public static function updateAlumno($obj) {
        global $DB;
        $returnValue = $DB->update_record('mtc_alumno', $obj);
        return $returnValue;
    }

    

}
