<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Service;

use matricula\Core\Template;
use matricula\Model\MatriculaModel;
use context_system;
use moodle_url;
use stdClass;
use matricula\Model\UtilModel;

class MatriculaService extends Template {

    private $valid_exts = ['pdf', 'PDF'];
    private $max_size = 200000 * 1024;
    private $path_certificado;
    private $utilService;

    public function __construct() {
        $this->valid_exts = ['pdf', 'PDF'];
    }

    public static function getMatriculasAll() {
        return MatriculaModel::getMatriculasAll();
        ;
    }

    public static function getMatriculasAllSemestre($id) {
        return MatriculaModel::getMatriculasAllSemestre($id);
        ;
    }

    public static function getSemestresAll() {
        return MatriculaModel::getSemestresAll();
        ;
    }

    public static function getNuevaMatricula() {
        global $USER;
        $obj = new \stdClass();
        //activo
        $obj->is_active = 0;
        $obj->int_alumnoid = 0;
        $obj->int_semestreid = 0;
        //eliminado
        $obj->is_deleted = 1;
        $obj->int_creatorid = $USER->id;
        $obj->date_timecreated = time();
        $obj->id = MatriculaModel::saveMatricula($obj);
        return $obj;
    }

    public static function getMatriculaById($id) {
        return MatriculaModel::getMatriculaById($id);
        ;
    }

    public static function getCursosAllDisponibles($objMatricula) {
        return MatriculaModel::getCursosAllDisponibles($objMatricula);
        ;
    }

    public static function getCursosAllSeleccionados($objMatricula) {
        return MatriculaModel::getCursosAllSeleccionados($objMatricula);
        ;
    }

    public function getUriEditMatricula($alumnoid) {
        return $this->routes()->generate('matriculas_editar', ['matriculaid' => $alumnoid]);
    }

    public function eliminarMatricula($idmatricula) {
        //catidad de alumnos
        MatriculaModel::UpdateCursoOcupado($idmatricula);
        //alumo libre
        MatriculaModel::UpdateAlumnoOcupado($idmatricula);
        //matricula cursos
        MatriculaModel::UpdateMatriculaCursoOcupado($idmatricula);
        //matricula
        $obj = new \stdClass();
        $obj->id = $idmatricula;
        //activo
        $obj->is_active = 0;
        //eliminado
        $obj->is_deleted = 1;
        MatriculaModel::updateMatricula($obj);
        return $idmatricula;
    }

    public function eliminarMatriculaCurso($idcurso, $idmatricula) {
        MatriculaModel::updateMatriculaCurso($idmatricula, $idcurso);
        $obj2 = \matricula\Model\CursoModel::getCursoById($idcurso);
        $obj2->int_cantidad_ocupada = $obj2->int_cantidad_ocupada - 1;
        $obj2->is_ocupado = 0;
        \matricula\Model\CursoModel::updateCurso($obj2);
        return $idmatricula;
    }

    public function GuardarMatriculaCurso($idmatricula, $idcursos) {
        $array = explode(",", $idcursos);
        $temporal = CursoService::getCursosById($idcursos);
        $noprocede = 0;
        foreach ($temporal as $curso) {
           foreach ($temporal as $temporal) {
                if($curso->chr_dia == $temporal->chr_dia && $curso->chr_horainicio == $temporal->chr_horainicio &&$curso->chr_horafin == $temporal->chr_horafin && $curso->id != $temporal->id ){
                    $noprocede = 1;
                }
           
        }
        }
        if ($noprocede == 0) {
            global $USER;
            foreach ($array as $curso) {
                $obj = new \stdClass();
                $obj->int_matriculaid = $idmatricula;
                $obj->int_cursoid = $curso;
                //activo
                $obj->is_active = 1;
                //eliminado
                $obj->is_deleted = 0;
                $obj->int_creatorid = $USER->id;
                $obj->date_timecreated = time();
                MatriculaModel::saveMatriculaCurso($obj);
                $obj2 = \matricula\Model\CursoModel::getCursoById($curso);
                $obj2->is_ocupado = 1;
                $obj2->int_cantidad_ocupada = $obj2->int_cantidad_ocupada + 1;
                \matricula\Model\CursoModel::updateCurso($obj2);
            }
        }

        return $noprocede;
    }

    public function GuardarMatricula($inputs) {
        $id = $inputs->get('matriculaid');
        global $USER;
        $obj = new \stdClass();
        //id
        $obj->id = $inputs->get('matriculaid');
        //nombre
        $obj->int_alumnoid = $inputs->get('inputAlumno');
        $obj->int_semestreid = $inputs->get('inputSemestre');
        $obj->is_active = 1;
        //eliminado
        $obj->is_deleted = 0;
        //creador
        //eliminado
        $obj->int_creatorid = $USER->id;

        $objA = new \stdClass();
        $objA->id = $inputs->get('inputAlumno');
        $objA->is_ocupado = 1;
        \matricula\Model\AlumnoModel::updateAlumno($objA);
        if ($id > 0) {
            //fecha modificacion registro
            $obj->date_timemodified = time();
            $returnValue = MatriculaModel::updateMatricula($obj);
        } else {
            //fecha creacion registro
            $obj->date_timecreated = time();
            $returnValue = MatriculaModel::saveMatricula($obj);
        }


        $returnValue = $inputs->get('matriculaid');
        return $returnValue;
    }

}
