<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Controller;

use matricula\Service\MatriculaService;
use matricula\Service\UtilService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use core\output\notification;
use context_system;
use moodle_url;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class MatriculaController extends MatriculaService {

    private $params;
    private $utilService;

    public function __construct() {
        $this->params = [];
    }

    /**
     * Vista para listar los registros de la tabla sílabos
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request) {
        //obtenemos las solicitudes
        $matriculas = $this->getMatriculasAll();
        $this->params['matriculas'] = $matriculas;
        $this->params['url_ajax'] = $this->routes()->generate('matriculas_ajax');
        $this->params['url_nueva_matricula'] = $this->routes()->generate('matriculas_editar');
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Matricula')->renderResponse('index.html.twig', $this->params);
    }

    public function editarAction(Request $request) {
        $this->params['title'] = "REGISTRAR NUEVA MATRICULA";
        //obtenemos el id
        $id = $request->attributes->get('matriculaid');
        //obtenemos el alumno
        if ($id < 1) {
            $objMatricula = $this->getNuevaMatricula();
        } else {
            $objMatricula = $this->getMatriculaById($id);
        }
        $cursos_disponibles = $this->getCursosAllDisponibles($objMatricula);
        $cursos_seleccionados = $this->getCursosAllSeleccionados($objMatricula);
        foreach ($cursos_seleccionados as $selecionados) {
        foreach ($cursos_disponibles as $diponibles) {
            if($selecionados->chr_dia == $diponibles->chr_dia && $selecionados->chr_horainicio == $diponibles->chr_horainicio &&$selecionados->chr_horafin == $diponibles->chr_horafin){
                $diponibles->bloqueado=1;
            }
        }
        }
        if ($objMatricula->int_alumnoid > 0) {
            $alumnos=[];
            $alumno = \matricula\Service\AlumnoService::getAlumnoById($objMatricula->int_alumnoid);
            array_push($alumnos, $alumno);
        } else {
            $alumnos = \matricula\Service\AlumnoService::getAlumnosAllNoOcupados();
        }
        $semestres = $this->getSemestresAll();
        //parametros
        $this->params['cursos_disponibles'] = $cursos_disponibles;
        $this->params['cursos_seleccionados'] = $cursos_seleccionados;
        $this->params['objMatricula'] = $objMatricula;
        $this->params['alumnos'] = $alumnos;
        $this->params['semestres'] = $semestres;
        $this->params['url_ajax'] = $this->routes()->generate('matriculas_ajax');
        $this->params['url_home'] = $this->routes()->generate('index');
        $this->params['url_backto'] = $this->routes()->generate('matriculas_index');
        return $this->template('Matricula')->renderResponse('editar.html.twig', $this->params);
    }

    public function ajaxAction(Request $request) {
        global $USER;
        $subject = $request->request->get('subject');
        $this->utilService = new UtilService();
        $error = 0;
        $message = '';
        switch ($subject) {
            case 'GuardarMatricula':
                $inputs = $request->request;
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $solicitudid = $this->GuardarMatricula($inputs);
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'id' => $solicitudid,
                    'url' => $this->routes()->generate('matriculas_editar', ['matriculaid' => $solicitudid])
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'GuardarCursosMatricula':
                $data = $request->request->get('data');
                $inputs = $request->request;
                $id = $inputs->get('matriculaid');
                if ($data != null) {
                    $solicitudid = $this->GuardarMatricula($inputs);
                    $docenteid = $this->GuardarMatriculaCurso($id, $data);
                }
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                if($docenteid==0){
                    
                }else{
                    $error=true;
                    $message= 'No puede tener dos cursos a la misma hora';
                }

                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'id' => $id,
                    'url' => $this->routes()->generate('matriculas_editar', ['matriculaid' => $id])
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deleteMatricula':
                $matriculaid = $request->request->get('matriculaid');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $this->eliminarMatricula($matriculaid);
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deleteMatriculaCurso':
                $cursoid = $request->request->get('data');

                $inputs = $request->request;
                $id = $inputs->get('matriculaid');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $this->eliminarMatriculaCurso($cursoid, $id);
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'id' => $id,
                    'url' => $this->routes()->generate('matriculas_editar', ['matriculaid' => $id])
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
        }
    }

    public function build_source_field($source) {
        $sourcefield = new \stdClass();
        $sourcefield->source = $source;
        return serialize($sourcefield);
    }

}
