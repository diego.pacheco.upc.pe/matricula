<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Controller;

use matricula\Service\CursoService;
use matricula\Service\UtilService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use core\output\notification;
use context_system;
use moodle_url;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class CursoController extends CursoService {

    private $params;
    private $utilService;

    public function __construct() {
        $this->params = [];
    }

    /**
     * Vista para listar los registros de la tabla sílabos
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request) {
        //obtenemos las solicitudes
        $cursos = $this->getCursosAll();
        //$modalidades = $this->getModalidadesMenu();
        $this->params['cursos'] = $cursos;
        $this->params['url_ajax'] = $this->routes()->generate('cursos_ajax');
        $this->params['url_editar'] = $this->routes()->generate('cursos_editar');
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Curso')->renderResponse('index.html.twig', $this->params);
    }

    public function editarAction(Request $request) {
        $this->params['title'] = "REGISTRAR NUEVO CURSO";
        //obtenemos el id
        $id = $request->attributes->get('cursoid');
        //obtenemos las carreras
        $carreras = \matricula\Service\AlumnoService::getCarrerasAll();
        //obtenemos los ciclos
        $ciclos = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
        $dias = array("Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");
        //obtenemos el alumno
        if ($id > 0) {
            $objCurso = $this->getCursoById($id);
        }
        //parametros
        $this->params['objCurso'] = $objCurso;
        $this->params['ciclos'] = $ciclos;
        $this->params['dias'] = $dias;
        $this->params['carreras'] = $carreras;
        $this->params['url_ajax'] = $this->routes()->generate('cursos_ajax');
        $this->params['url_backto'] = $this->routes()->generate('cursos_index');
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Curso')->renderResponse('editar.html.twig', $this->params);
    }

    public function ajaxAction(Request $request) {
        global $USER;
        $subject = $request->request->get('subject');
        $this->utilService = new UtilService();
        $error = 0;
        $message = '';
        switch ($subject) {
            case 'GuardarCurso':
                $fallo = 0;
                $fallo2 = 0;
                $inputs = $request->request;
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $id = $inputs->get('cursoid');
                if ($id != null) {
                    $objAlumnoid = $this->getCursoById($id);
                }
                $dni = $inputs->get('inputTxtSeccion');
                $chr_correo = $inputs->get('inputTxtCodigo');
                $objAlumno = $this->getCursoBySeccion($dni);
                $objAlumnocorreo = $this->getCursoByCodigo($chr_correo);
                if ($objAlumnoid->id == $objAlumno->id && $objAlumnoid->id == $objAlumnocorreo->id && $objAlumnoid->chr_seccion == $objAlumno->chr_seccion && $objAlumnoid->chr_code == $objAlumnocorreo->chr_code) {
                    $solicitudid = $this->GuardarCurso($inputs);
                    $response['success'] = true;
                } else {
                    if ($objAlumnoid->id == $objAlumno->id) {
                        $fallo1 = 0;
                    } else {
                        if ($inputs->get('inputTxtSeccion') != $objAlumno->chr_seccion) {
                            $fallo1 = 0;
                        } else {
                            $message .= 'La seccion ya existe. ';
                            $fallo1 = 1;
                            $error = true;
                        }
                    }

                    if ($objAlumnoid->id == $objAlumnocorreo->id) {
                        $fallo2 = 0;
                    } else {
                        if ($inputs->get('inputTxtCodigo') != $objAlumnocorreo->chr_code) {
                            $fallo2 = 0;
                        } else {
                            $message .= 'El codigo  ya existe. ';
                            $fallo2 = 1;
                            $error = true;
                        }
                    }

                    if ($fallo == 0 && $fallo2 == 0) {
                        $solicitudid = $this->GuardarCurso($inputs);
                        $response['success'] = true;
                    }
                }
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'id' => $solicitudid,
                    'url' => $this->routes()->generate('cursos_index')
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deleteCurso':
                $cursoid = $request->request->get('cursoid');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $validado = $this->getCursoById($cursoid);
                if ($validado->is_ocupado == 0) {
                    $this->eliminarCurso($cursoid);
                } else {
                    $message = 'No se puede eliminar un curso con alumnos matriculados';
                    $error = true;
                }

                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
        }
    }

    public function build_source_field($source) {
        $sourcefield = new \stdClass();
        $sourcefield->source = $source;
        return serialize($sourcefield);
    }

}
