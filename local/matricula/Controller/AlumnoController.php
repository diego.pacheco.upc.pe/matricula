<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Controller;

use matricula\Service\AlumnoService;
use matricula\Service\UtilService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use core\output\notification;
use context_system;
use moodle_url;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class AlumnoController extends AlumnoService {

    private $params;
    private $utilService;

    public function __construct() {
        $this->params = [];
    }

    /**
     * Vista para listar los registros de la tabla sílabos
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request) {
        //obtenemos las solicitudes
        $alumnos = $this->getAlumnosAll();
        //$modalidades = $this->getModalidadesMenu();
        $this->params['alumnos'] = $alumnos;
        $this->params['url_ajax'] = $this->routes()->generate('alumnos_ajax');
        $this->params['url_nuevo_alumno'] = $this->routes()->generate('alumnos_editar');
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Alumno')->renderResponse('index.html.twig', $this->params);
    }

    public function editarAction(Request $request) {
        $this->params['title'] = "REGISTRAR NUEVO ALUMNO";
        //obtenemos el id
        $id = $request->attributes->get('alumnoid');
        //obtenemos las carreras
        $carreras = $this->getCarrerasAll();
        //obtenemos los ciclos
        $ciclos = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
        //obtenemos el alumno
        if ($id > 0) {
            $objAlumno = $this->getAlumnoById($id);
        }
        //parametros
        $this->params['objAlumno'] = $objAlumno;
        $this->params['ciclos'] = $ciclos;
        $this->params['carreras'] = $carreras;
        $this->params['url_ajax'] = $this->routes()->generate('alumnos_ajax');
        $this->params['url_home'] = $this->routes()->generate('index');
        $this->params['url_backto'] = $this->routes()->generate('alumnos_index');
        return $this->template('Alumno')->renderResponse('editar.html.twig', $this->params);
    }

    public function ajaxAction(Request $request) {
        global $USER;
        $subject = $request->request->get('subject');
        $this->utilService = new UtilService();
        $error = 0;
        $message = '';
        switch ($subject) {
            case 'GuardarAlumno':
                $fallo1 = 0;
                $fallo2 = 0;
                $fallo3 = 0;
                $fallo4 = 0;
                $inputs = $request->request;
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $id = $inputs->get('alumnoid');
                if ($id != null) {
                    $objAlumnoid = $this->getAlumnoById($id);
                }
                $dni = $inputs->get('inputTxtDNI');
                $chr_cod_alumno = $inputs->get('inputTxtCodeEstudiante');
                $chr_correo = $inputs->get('inputTxtcorreo');
                //celular
                $int_celular = $inputs->get('inputTxtcelular');
                $objAlumno = $this->getAlumnoByDNI($dni);
                $objAlumnocode = $this->getAlumnoBycode($chr_cod_alumno);
                $objAlumnocorreo = $this->getAlumnoByCorreo($chr_correo);
                $objAlumnocelular = $this->getAlumnoByCelular($int_celular);
                $objAlumnoDocenteDni = \matricula\Service\DocenteService::getDocenteByDNI($dni);
                $objAlumnoDocentecorreo = \matricula\Service\DocenteService::getDocenteByCorreo($chr_correo);
                $objAlumnoDocentecelular = \matricula\Service\DocenteService::getDocenteByCelular($int_celular);

                if ($objAlumnoid->id == $objAlumno->id && $objAlumnoid->id == $objAlumnocorreo->id && $objAlumnoid->id == $objAlumnocelular->id && $objAlumnoid->id == $objAlumnocode->id && $objAlumnoid->chr_cod_alumno == $objAlumnocode->chr_cod_alumno && $objAlumnoid->int_dni == $objAlumno->int_dni && $objAlumnoid->chr_correo == $objAlumnocorreo->chr_correo && $objAlumnoid->int_celular == $objAlumnocelular->int_celular && $objAlumnoid->int_dni != $objAlumnoDocenteDni->int_dni && $objAlumnoid->chr_correo != $objAlumnoDocentecorreo->chr_correo && $objAlumnoid->int_celular != $objAlumnoDocentecelular->int_celular) {
                    $solicitudid = $this->GuardarAlumno($inputs);
                    $response['success'] = true;
                } else {
                    if ($objAlumnoid->id == $objAlumnocode->id) {
                        if ($inputs->get('inputTxtCodeEstudiante') != $objAlumnocode->chr_cod_alumno) {
                            $fallo1 = 0;
                        } else {
                            $message .= 'El Codigo de estudiante ya existe. ';
                            $fallo1 = 1;
                            $error = true;
                        }
                    } else {

                        if ($inputs->get('inputTxtCodeEstudiante') != $objAlumnocode->chr_cod_alumno) {
                            $fallo1 = 0;
                        } else {
                            $message .= 'El Codigo de estudiante ya existe. ';
                            $fallo1 = 1;
                            $error = true;
                        }
                    }


                    if ($objAlumnoid->id == $objAlumno->id) {
                        if ($inputs->get('inputTxtDNI') != $objAlumnoDocenteDni->int_dni) {
                            $fallo2 = 0;
                        } else {
                            $message .= 'El DNI ya existe. ';
                            $fallo2 = 1;
                            $error = true;
                        }
                    } else {
                        if ($inputs->get('inputTxtDNI') != $objAlumno->int_dni && $inputs->get('inputTxtDNI') != $objAlumnoDocenteDni->int_dni) {
                            $fallo2 = 0;
                        } else {
                            $message .= 'El DNI ya existe. ';
                            $fallo2 = 1;
                            $error = true;
                        }
                    }

                    if ($objAlumnoid->id == $objAlumnocorreo->id) {
                        if ($inputs->get('inputTxtcorreo') != $objAlumnoDocentecorreo->chr_correo) {
                            $fallo3 = 0;
                        } else {
                            $message .= 'El correo  ya existe. ';
                            $fallo3 = 1;
                            $error = true;
                        }
                    } else {
                        if ($inputs->get('inputTxtcorreo') != $objAlumnocorreo->chr_correo && $inputs->get('inputTxtcorreo') != $objAlumnoDocentecorreo->chr_correo) {
                            $fallo3 = 0;
                        } else {
                            $message .= 'El correo  ya existe. ';
                            $fallo3 = 1;
                            $error = true;
                        }
                    }
                    if ($objAlumnoid->id == $objAlumnocelular->id) {
                        if ($inputs->get('inputTxtcelular') != $objAlumnoDocentecelular->int_celular) {
                            $fallo4 = 0;
                        } else {
                            $message .= 'El celular  ya existe. ';
                            $fallo4 = 1;
                            $error = true;
                        }
                    } else {
                        if ($inputs->get('inputTxtcelular') != $objAlumnocelular->int_celular && $inputs->get('inputTxtcelular') != $objAlumnoDocentecelular->int_celular) {
                            $fallo4 = 0;
                        } else {
                            $message .= 'El celular  ya existe. ';
                            $fallo4 = 1;
                            $error = true;
                        }
                    }
                    if ($fallo1 == 0 && $fallo2 == 0 &&  $fallo3 == 0 && $fallo4 == 0) {
                        $solicitudid = $this->GuardarAlumno($inputs);
                        $response['success'] = true;
                    }
                }
                $response['data'] = [
                    'id' => $solicitudid,
                    'url' => $this->routes()->generate('alumnos_index')
                ];
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deleteAlumno':
                $alumnoid = $request->request->get('alumnoid');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $matriculaid = $this->getMatriculaidByAlumnoid($alumnoid);
                if($matriculaid!= null){
                    \matricula\Service\MatriculaService::eliminarMatricula($matriculaid);
                }
                $this->eliminarAlumno($alumnoid);
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
        }
    }

    public function build_source_field($source) {
        $sourcefield = new \stdClass();
        $sourcefield->source = $source;
        return serialize($sourcefield);
    }

}
